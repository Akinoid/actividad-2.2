﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            string restart;
            
            do
            {

                int respuesta;
                Console.WriteLine("Que figura desea operar : \n" +
                    "1. Rectangulo \n" +
                    "2. Cuadrado\n" +
                    "3. Circulo \n" +
                    "4. Triangulo\n" +
                    "5. Rombo");
                    
                respuesta = int.Parse(Console.ReadLine());

                switch (respuesta)
                {
                    case 1:
                        Console.WriteLine("Introduce el lado 1 (cm)");
                        float Lado1 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Introduce el lado 2 (cm)");
                        float Lado2 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Introduce el Nombre del Rectangulo");
                        string NombreRec = Console.ReadLine();
                        Rectangulo rectangulo = new Rectangulo(NombreRec, Lado1, Lado2);
                        Console.WriteLine("Que operacion va a operar 1. Area o 2. Perimetro (1/2)");
                        int Opcion = int.Parse(Console.ReadLine());
                        switch(Opcion)
                        {
                            case 1:
                                rectangulo.CalculateArea();
                                break;
                            case 2:
                                rectangulo.CalculatePerimetro();
                                break;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Introduce el lado 1 (cm)");
                        float Lado1C = float.Parse(Console.ReadLine());                      
                        Console.WriteLine("Introduce el Nombre del Rectangulo");
                        string NombreCu = Console.ReadLine();
                        Cuadrado cuadrado = new Cuadrado(NombreCu, Lado1C);
                        Console.WriteLine("Que operacion va a operar 1. Area o 2. Perimetro (1/2)");
                        int OpcionC = int.Parse(Console.ReadLine());
                        switch (OpcionC)
                        {
                            case 1:
                                cuadrado.CalculateArea();
                                break;
                            case 2:
                                cuadrado.CalculatePerimetro();
                                break;
                        }
                        break;
                    case 3:
                        Console.WriteLine("Introduce el radio (cm)");
                        float radio = float.Parse(Console.ReadLine());
                        Console.WriteLine("Introduce el lado 2 (cm)");
                        Console.WriteLine("Introduce el Nombre del Rectangulo");
                        string NombreCir = Console.ReadLine();
                        Circulo circulo = new Circulo(NombreCir,radio);
                        Console.WriteLine("Que operacion va a operar 1. Area o 2. Perimetro (1/2)");
                        int OpcionCir = int.Parse(Console.ReadLine());
                        switch (OpcionCir)
                        {
                            case 1:
                                circulo.CalculateArea();
                                break;
                            case 2:
                                circulo.CalculatePerimetro();
                                break;
                        }                       
                        break;
                    case 4:
                        Console.WriteLine("Introduce el lado 1 (cm)");
                        float Lado1T = float.Parse(Console.ReadLine());
                        Console.WriteLine("Introduce el lado 2 (cm)");
                        float Lado2T = float.Parse(Console.ReadLine());
                        Console.WriteLine("Introduce el lado 3 (cm)");
                        float Lado3T = float.Parse(Console.ReadLine());
                        Console.WriteLine("Introduce el Nombre del Rectangulo");
                        string NombreTri = Console.ReadLine();
                        Triangulo triangulo = new Triangulo(NombreTri, Lado1T, Lado2T, Lado3T);
                        Console.WriteLine("Que operacion va a operar 1. Area o 2. Perimetro (1/2)");
                        int OpcionT = int.Parse(Console.ReadLine());
                        switch (OpcionT)
                        {
                            case 1:
                                triangulo.CalculateArea();
                                break;
                            case 2:
                                triangulo.CalculatePerimetro();
                                break;
                        }
                        break;
                    case 5:
                        Console.WriteLine("Introduce el lado 1 (cm)");
                        float Lado1R = float.Parse(Console.ReadLine());                 
                        Console.WriteLine("Introduce el Nombre del Rectangulo");
                        string NombreRom = Console.ReadLine();
                        Rombo rombo = new Rombo(NombreRom, Lado1R);
                        Console.WriteLine("Que operacion va a operar 1. Area o 2. Perimetro (1/2)");
                        int OpcionR = int.Parse(Console.ReadLine());
                        switch (OpcionR)
                        {
                            case 1:
                                rombo.CalculateArea();
                                break;
                            case 2:
                                rombo.CalculatePerimetro();
                                break;
                        }
                        break;

                   
                }

                Console.WriteLine("Quieres otra figura (YES/NO)");
                restart = Console.ReadLine().ToUpper();
                while ((restart != "YES") && (restart != "NO"))
                {
                    Console.WriteLine("Error");
                    Console.WriteLine("Do you wish to calculate another? (YES/NO) ");
                    restart = Console.ReadLine().ToUpper();
                }

            } while (restart == "YES");
        }
    }
}
