﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad_2._2
{
    internal class Rombo
    {
        private float lado;
        private string name;
        private string type;

        public Rombo(string name, float lado)
        {
            this.lado = lado;
            this.name = name;
            this.type = "rombo";
        }

        public float CalculateArea()
        {
            return lado * lado * (float)Math.Sin(Math.PI / 180 * 45);
        }

        public float CalculatePerimetro()
        {
            return lado * 4;
        }

        public string GetName()
        {
            return name;
        }

        public string ReturnType()
        {
            return type;
        }
    }
}
