using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad_2._2
{
    class Cuadrado
    {
        private float a;
        private string name;
        private string type;

        public Cuadrado(string name, float a)
        {
            this.a = a;
            this.name = name;
            this.type = "Cuadrado";
        }

        public float CalculateArea()
        {
            return a * a * a * a;
        }
        public float CalculatePerimetro()
        {
            return 4 * a;
        }
        public string GetName()
        {
            return name;
        }
        public string ReturnType()
        {
            return type;
        }     
    }
}
