﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad_2._2
{
    class Circulo
    {
        private float radius;
        private string name;
        private string type;

        public Circulo(string name, float radius)
        {
            this.radius = radius;
            this.name = name;
            this.type = "Circle";
        }

        public float CalculateArea()
        {
            return radius * radius * 3.14f;
        }
        public float CalculatePerimetro()
        {
            return radius * (2* (3.14f));
        }

        public string GetName()
        {
            return name;
        }

        public string ReturnType()
        {
            return type;
        }
    }
}
