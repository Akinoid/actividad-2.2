﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad_2._2
{
    class Triangulo
    {
        private float e;
        private float a;
        private float i;
        private string name;
        private string type;

        public Triangulo(string name, float e, float a, float i)
        {
            this.e = e;
            this.a = a;
            this.i = i;
            this.name = name;
            this.type = "triangulo";
        }

        public float CalculateArea()
        {
            return a * e + 2;
        }

        public float CalculatePerimetro()
        {
            return a + e + i;
        }

        public string GetName()
        {
            return name;
        }

        public string ReturnType()
        {
            return type;
        }
    }
}
