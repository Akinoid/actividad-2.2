﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad_2._2
{
    class Rectangulo
    {
        private float a;
        private float b;
        private string name;
        private string type;

        public Rectangulo(string name, float a, float b)
        {
            this.a = a;
            this.b = b;
            this.name = name;
            this.type = "Rectangulo";
        }

        public float CalculateArea()
        {
            return a * b;
        }
        public float CalculatePerimetro()
        {
            return 2 * a + 2 * b;
        }
        public string GetName()
        {
            return name;
        }

        public string ReturnType()
        {
            return type;
        }
    }
}

